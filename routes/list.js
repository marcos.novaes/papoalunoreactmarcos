const express = require('express')
const router = express.Router()
const List = require('../models/list')
const list = require('../models/list')


//pega todas as listas
router.get('/', async (req, res) => {
    try {
        const lists = await List.find({})
        if (lists.length > 0) {
            res.status(200).json(lists)
        }
        else {
            res.status(400).send({ "erro": "nao tem listas" })
        }

    } catch (error) {
        console.log(error.message)
        res.status(500).send({ "Erro::": error.message })
    }
})


// //pega receita especifica
// router.get('/:id', [], async (req, res, next) => {
//     try {
//         // const receita = await Recipe.findById(req.params.id)
//         // if (receita) {
//         //     res.status(200).json(receita)
//         // }
//         // else {
//         //     res.status(400).send({ "Erro:": "receita nao encontrada" })
//         // }

//     } catch (error) {
//         console.log(error.message)
//         res.status(500).send({ "Erro::": error.message })
//     }
// })




//cria receita nova
router.post('/new', async (req, res) => {
    console.log('ta no post')
    try {
        const { nome, items } = req.body
        let list = new List({ nome, items })
        await list.save()
        res.status(200).send(list)
    } catch (error) {
        console.log(error.message)
        res.status(500).send({ "Erro::": error.message })
    }

})


//deleta receita
router.delete("/:id", async (req, res) => {
    try {
        const listaDeletada = await List.findByIdAndDelete(req.params.id)
        if (listaDeletada) {
            res.status(200).json(listaDeletada)
        }
        else {
            res.status(400).send({ "Erro": "não foi possivel achar a lista" })
        }
    } catch (error) {
        res.status(500).send({ "Erro::": error.message })
    }
})

//edita receita com put
router.put("/:id", async (req, res, next) => {
    try {
        const { nome, items } = req.body
        const listEditada = await List.findByIdAndUpdate(req.params.id, { nome, items }, { new: true })
        if (listEditada) {
            res.status(200).send(listEditada)
        }
        else {
            res.status(404).send("não encontramos a lista a ser editada")
        }
    } catch (error) {
        console.log(error.message)
        res.status(500).send({ "Erro::": error.message })
    }

})

//edita receita com patch
router.patch('/:id', async (req, res, next) => {
    try {
        // const update = req.body
        // const recipeEditada = await Recipe.findByIdAndUpdate(req.params.id, update, { new: true })
        // if (recipeEditada) {
        //     res.status(200).json(recipeEditada)
        // }
        // else {
        //     res.status(400).send({ "Erros:": "receita nao encontrada" })
        // }
    } catch (error) {
        console.log(error.message)
        res.status(500).send({ "Erro::": error.message })
    }
})

module.exports = router