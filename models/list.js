const mongoose = require('mongoose')

const ListSchema = new mongoose.Schema({
    nome: {
        type: String,
        required: true
    },
    items: [{
        descricao: String,
        urgente: Boolean,
        feito: Boolean
    }]
})

module.exports = mongoose.model("list", ListSchema)

